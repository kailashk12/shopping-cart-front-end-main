import React from 'react';
import axios from 'axios';

class Connection extends React.Component {
    state ={
        details: []
    };
    componentDidMount() {
        let data;
        console.log("mounted")
        axios.get("http://127.0.0.1:8000/rest/products").then((res) => {
            data = res.data;
            this.setState({
                details:data,
                
            });
        })
        .catch((err) => {

        });
    }
    render() 
    {
        return (
        this.state.details.map((detail,id) => (
                <div key={id}>
                <h1>{detail.id}</h1>
                <h2>{detail.description}</h2>
                <h2>{detail.price}</h2>
                <h2>{detail.created_at}</h2>
                <h2>{detail.updated_at}</h2>
                <img src={detail.image} alt="Girl in a jacket" width="500" height="600"></img>
                
                </div>
            ))
        
        );
    }

}

export default Connection