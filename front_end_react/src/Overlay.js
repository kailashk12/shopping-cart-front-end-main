import styled from "styled-components";

export const Overlay = styled.div`
  background-color: rgba(255, 0, 0, 0.4);
  position: fixed;
  z-index: 3;
  height: 100vh;
  width: 100vw;
  overflow: hidden;
  display: ${({ open }) => (open ? "block" : "none")};
`;
