import { createGlobalStyle } from "styled-components";
export const lightGray = "#fffre";
export const GlobalStyles = createGlobalStyle`
 * {
   font-family: Times, serif;
  margin: 0;
}
 body {
	background: linear-gradient(-45deg, #ee7752, #e73c7e, #23a6d5, #23d5ab);
	background-size: 400% 400%;
	animation: gradient 5s ease infinite;
}

@keyframes gradient {
	0% {
		background-position: 0% 50%;
	}
	50% {
		background-position: 100% 50%;
	}
	100% {
		background-position: 0% 50%;
	}
}
`;
