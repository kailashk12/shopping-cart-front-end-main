import React from "react";
import { numberFormat } from "./numberFormat";
import { VerticalBar } from "./VerticalBar";
import styled from "styled-components";

export const CartTotals = ({ cart, cartCountTotal }) => {
  const cartPriceTotal = cart.reduce(
    (acc, item) => acc + item.price * item.quantity,
    0
  );

  return (
    <H2>
      Items: {cartCountTotal} <VerticalBar /> Total Price: RS
      {numberFormat(cartPriceTotal)}
    </H2>
  );
};

const H2 = styled.h2`
  padding: 8px 0;
  font-size: 47px;
  border-bottom: 4px dashed black;
`;
