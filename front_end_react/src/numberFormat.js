// It will show cart number increasing and decreasing

export const numberFormat = val =>
  Number.isInteger(val) ? val : val.toFixed(2);
