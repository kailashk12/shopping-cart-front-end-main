import styled from "styled-components";
import React from "react";
const Styles = styled.span`
  margin: 0 3px;
`;

export const VerticalBar = () => <Styles>&#124;</Styles>;
